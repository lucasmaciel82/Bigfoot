#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <queue>
#include <iostream>
#include <algorithm>
#include <math.h>

using namespace std;

#define EPS 1e-7

int cmpf(double a, double b = 0);

vector<string> explode(const string& s, const string& m);

vector<unsigned int> merge(const vector<unsigned int>& v1, const vector<unsigned int>& v2);

#endif
