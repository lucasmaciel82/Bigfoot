#include "n-set-corr.h"

multiple_nsets<uint> readNSets(istream& input, vector<uint>& sizes) throw (IncorrectNbOfDimensionsException){
	multiple_nsets<uint> nSets;
	char_separator<char> dimensionSeparator(" ");
	char_separator<char> valueSeparator(",");

	while (input.good()){
		single_nset<uint> nSet;
		nSet.reserve(sizes.size());

		string nSetString;
		getline(input, nSetString);

		tokenizer<char_separator<char> > dimensions(nSetString, dimensionSeparator);
		tokenizer<char_separator<char> >::iterator dimensionIt = dimensions.begin();

		if (dimensionIt != dimensions.end()){

			for (auto& sizeIt : sizes){
				if (dimensionIt == dimensions.end()){
					throw IncorrectNbOfDimensionsException(nSetString, sizes.size());
				}

				set<uint> dimension;
				tokenizer<char_separator<char> > values(*dimensionIt, valueSeparator);
				for (const auto& valueIt : values){
					const uint value = lexical_cast<uint>(valueIt.c_str());
					if (value > sizeIt){
						sizeIt = value;
					}
					dimension.insert(value);
				}

				nSet.push_back(dimension);
				++dimensionIt;
			}

			if (dimensionIt != dimensions.end()){
				throw IncorrectNbOfDimensionsException(nSetString, sizes.size());
			}
			nSets.push_back(nSet);
		}
	}
	return nSets;
}

uint totalNumberOfTuples(single_nset<uint> nSet){
	uint ans = 1;
	for (const auto& it : nSet)
		ans *= it.size();
	return ans;
}

single_nset<uint> nSetIntersection(single_nset<uint>& first, single_nset<uint>& second){
	single_nset<uint> intersect(first.size());

	single_nset<uint>::iterator dimensionIt = intersect.begin(), secondIt = second.begin();
	for (const auto& it : first){
		for (const auto& valueIt : it)
			if (secondIt->find(valueIt) != secondIt->end())
				dimensionIt->insert(valueIt);
		dimensionIt++;
		secondIt++;
	}

	return intersect;
}

void getNTuplesFromNSet(const single_nset<uint>::const_iterator& dimensionIt, const single_nset<uint>::const_iterator& dimensionEnd, vector<uint>& prefix, vector<vector<uint> >& nTuples){
	if (dimensionIt == dimensionEnd){
		nTuples.push_back(prefix);
	}
	else{
		const single_nset<uint>::const_iterator nextDimensionIt = dimensionIt + 1;
		for (const auto& valueIt : *dimensionIt){
			prefix.push_back(valueIt);
			getNTuplesFromNSet(nextDimensionIt, dimensionEnd, prefix, nTuples);
			prefix.pop_back();
		}
	}
}

vector<vector<uint> > getNTuplesFromNSet(const single_nset<uint>::const_iterator& dimensionBegin, const single_nset<uint>::const_iterator& dimensionEnd){
	vector<vector<uint> > nTuples;
	vector<uint> prefix;
	prefix.reserve(dimensionEnd - dimensionBegin);
	getNTuplesFromNSet(dimensionBegin, dimensionEnd, prefix, nTuples);
	return nTuples;
}

void nSetUnion(ntuples<uint>& ans, single_nset<uint>& nset){
	vector< vector<uint> > nTuples = getNTuplesFromNSet(nset.begin(), nset.end());
	for (const auto& t : nTuples){
		ans.insert(t);
	}
}

double computeCorrelation(single_nset<uint>& first, single_nset<uint>& second){
	single_nset<uint> intersect = nSetIntersection(first, second);
	ntuples<uint> _union;
	nSetUnion(_union, first);
	nSetUnion(_union, second);
	double v1 = totalNumberOfTuples(intersect), v2 = _union.size();
	return v1 / v2;
}

void show(single_nset<uint> &nSet){
	for (const auto& dimensionIt: nSet){
		for (const auto& value : dimensionIt){
			if (value != *dimensionIt.begin())
				cout << ",";
			cout << value;
		}
		cout << " ";
	}
	cout << endl;
}

void show(ntuples<uint> &nTuples){
	for (const auto& tupleIt : nTuples){
		for (const auto& vlrIt : tupleIt){
			cout << vlrIt << " ";
		}
		cout << endl;
	}
}

int main(int argc, char* argv[]){

	double VERBOSE = false, AVERAGE = false;

	po::options_description generic("Generic Options");
	generic.add_options()
		("extracted-patterns-file", po::value<string>(), "extracted patterns file")
		("hidden-patterns-file", po::value<string>(), "original hidden patterns file");

	po::options_description opts("Options");
	opts.add_options()
		("help", "produce help message")
		("verbose,v", "write similarity in a human friendly interface")
		("average,avg", "answer is the average of the patterns similarity");

	po::options_description all;
	all.add(generic).add(opts);

	po::positional_options_description mandatory;
	mandatory.add("extracted-patterns-file", 1);
	mandatory.add("hidden-patterns-file", 1);
	mandatory.add("help", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(all).positional(mandatory).run(), vm);
	po::notify(vm);

	if (vm.count("help") || !vm.count("extracted-patterns-file") || !vm.count("hidden-patterns-file")){
		cout << "Usage: simodel [Options] extracted-patterns-file hidden-patterns-file" << endl;
		cout << generic << opts;
		return 1;
	}

	string urlfile1 = "", urlfile2 = "";
	if (vm.count("extracted-patterns-file")){
		urlfile1 = vm["extracted-patterns-file"].as< string >();
	}

	if (vm.count("hidden-patterns-file")){
		urlfile2 = vm["hidden-patterns-file"].as< string >();
	}

	if (vm.count("verbose")){
		VERBOSE = true;
	}

	if (vm.count("average")){
		AVERAGE = true;
	}

	ifstream file1(urlfile1.c_str());
	if (file1.fail()){
		cerr << NoFileException(urlfile1.c_str()).what() << endl;
		return EX_IOERR;
	}

	char_separator<char> dimensionSeparator(" ");

	uint n = 0;
	while (n == 0 && file1.good()){
		string nSetString;
		getline(file1, nSetString);

		tokenizer<char_separator<char> > dimensions(nSetString, dimensionSeparator);
		for (const auto& dimensionIt : dimensions){
			++n;
		}
	}

	vector<uint> sizes(n, 0);
	file1.seekg(0, ios::beg);

	ifstream file2(urlfile2.c_str());
	if (file2.fail()){
		cerr << NoFileException(urlfile2.c_str()).what() << endl;
		return EX_IOERR;
	}

	multiple_nsets<uint> nSets1;
	multiple_nsets<uint> nSets2;
	try{
		nSets1 = readNSets(file1, sizes);
		nSets2 = readNSets(file2, sizes);
	}
	catch (IncorrectNbOfDimensionsException e){
		cerr << e.what();
		return EX_DATAERR;
	}

	ntuples<uint> all_union, all_intersect;


	if (VERBOSE){
		cout.precision(4);

		cout << "\tExtracted Patterns\tHidden Patterns\t\tSimilarity" << endl;
	}

	int i = 0;
	double total_avg = 0;
	for (auto& nSetIt2 : nSets2){
		single_nset<uint> *id;
		double best = -1;
		int j = 0, k = 0;
		for (auto& nSetIt1 : nSets1){
			double vlr = computeCorrelation(nSetIt1, nSetIt2);
			if (vlr > best || (vlr == best && *id < nSetIt1)){
				best = vlr;
				id = &nSetIt1;
				j = k;
			}
			k++;
		}
		auto aux = nSetIntersection(nSetIt2, *id);
		nSetUnion(all_intersect, aux);
		nSetUnion(all_union, nSetIt2);

		total_avg += best;

		if (VERBOSE){
			cout << fixed << "\tPattern " << j+1 << "\t\tPattern " << i+1 << "\t\t" << best << endl;
		}
		i++;
	}

	for (auto& nSetIt1: nSets1){
		nSetUnion(all_union, nSetIt1);
	}

	double v1 = all_intersect.size(), v2 = all_union.size();

	if (VERBOSE){
		cout << fixed << "\nModel Similarity: ";
	}

	if (AVERAGE){
		cout << total_avg / nSets2.size() << endl;
	}
	else{
		cout << v1 / v2 << endl;
	}

	return EX_OK;
}
