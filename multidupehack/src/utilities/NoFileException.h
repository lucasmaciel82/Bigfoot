// Copyright (C) 2013-2018 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of multidupehack.

// multidupehack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// multidupehack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with multidupehack.  If not, see <https://www.gnu.org/licenses/>.

#ifndef NOFILEEXCEPTION_H_
#define NOFILEEXCEPTION_H_

#include <exception>
#include <string>

using namespace std;

class NoFileException final : public std::exception
{
 public:
  NoFileException(const char* fileName);
  const char* what() const noexcept;

 protected:
  string message;
};

#endif /*NOFILEEXCEPTION_H_*/
