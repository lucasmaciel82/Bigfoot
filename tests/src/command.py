import configparser
import copy
import json
import os
import stat
import time
import shlex
import re
from subprocess import Popen, PIPE

from src.utils import createfile, validfile, printstyle, sh


def createshfile(c):
    # cria sh com permissão de execução
    createfile(c['sh'], c['sh_content'])
    st = os.stat(c['sh'])
    os.chmod(c['sh'], st.st_mode | stat.S_IEXEC)


def createfolders(c):
    # cria as pastas
    if not os.path.exists(os.path.dirname(c['stdout'])):
        os.makedirs(os.path.dirname(c['stdout']))


def str_to_bool(s):
    if s == 'True' or s == 'true':
         return True
    elif s == 'False' or s == 'false':
         return False
    else:
         raise ValueError # evil ValueError that doesn't tell you what the wrong value was


def have_error_in_other_execution(c,args):
    mask = re.sub('[0-9][0-9]test', '*', c['stderr'])
    errs = sh('ls ' + mask)
    if errs:
        for err in errs:
            if validfile(err):
                return True
    return False


class Command(dict):
    @staticmethod
    def create_all_commands(commands_config_file):
        commands_config = configparser.ConfigParser()
        commands_config._interpolation = configparser.ExtendedInterpolation()
        commands_config.read(commands_config_file)

        command_list = dict()
        for command_name in filter(lambda x: x.startswith('Command'), commands_config):
            command_list[json.loads(commands_config[command_name]['name'])] = Command(commands_config, command_name)

        return command_list

    def __init__(self, config, command_name):
        super().__init__()

        self.arguments = None
        for command_var in config[command_name]:
            self[command_var] = json.loads(config[command_name][command_var])

        # include fields in cascading overlay
        for global_var in config['Global']:
            self[global_var] = json.loads(config['Global'][global_var])
        for local_var in config[command_name]:
            self[local_var] = json.loads(config[command_name][local_var])

    def update_params(self, field_list):
        if isinstance(field_list, str):
            field_list = [field_list]

        for var in [k for k, v in self.items() if "%{" in str(v)]:
            for k, v in self.items():
                self[var] = self[var].replace('%{' + k + '}', str(v))
            for k, v in self.arguments.items():
                self[var] = self[var].replace('%{' + k + '}', str(v))

        for field_name in [field for field in field_list if "%{" in str(self[field])]:
            if "%{" in self[field_name]:
                self.update_params(field_list)

    def copy_with_arguments(self, arguments):
        self.arguments = arguments
        arguments.update_strings()

        c = copy.deepcopy(self)
        if 'previous-output-error' in c :
            c.update_params(['sh', 'sh_content', 'stdout', 'stderr', 'run', 'previous-output-error'])
        else:
            c.update_params(['sh', 'sh_content', 'stdout', 'stderr', 'run'])

        return c

    def run(self, arguments):

        c = self.copy_with_arguments(arguments)

        if validfile(c['stdout']):
            # printstyle(c['name'], "Já executado", c['stdout'])
            return c
        if validfile(c['stderr']):
            # printstyle(c['name'], "Já executado com erro", c['stderr'])
            return c
        if validfile(c['sh']):
            # printstyle(c['name'], 'Em execução ou .sh já criado', c['stdout'])
            return c
        if 'previous-output-error' in c and validfile(c['previous-output-error']):
            open(c['stderr'], 'w').write('Execução prévia com erro. ' + '\n')
            # printstyle(c['name'], 'Arquivo de entrada com erro, c['previous-output-error'])
            return c
        if have_error_in_other_execution(c, arguments):
            open(c['stderr'], 'w').write('Não executado: Configuração atual já obteve erro. ' + '\n')
            return c

        createshfile(c)
        createfolders(c)

        printstyle(c['name'], "Start execution of %s %s" % (c['name'], c['stdout']))

        start = time.time()

        if ('docker' in c):
                if str_to_bool(c['docker']):
                        p = Popen(shlex.split(c['docker-run']), stdout=PIPE, stderr=PIPE)
                else:
                        p = Popen(shlex.split(c['run']), stdout=PIPE, stderr=PIPE)
        else:
                p = Popen(shlex.split(c['run']), stdout=PIPE, stderr=PIPE)

        output, error = p.communicate()
        end = time.time()

        if p.returncode == 125 and 'docker: Error response from daemon: Conflict.' in str(error):
            printstyle(c['name'], "Exec in %f: Signal 125: Nome do docker já em uso" % (end - start), c['stderr'])
            return c

        if p.returncode == 124:
            open(c['stderr'], 'w').write('Signal 124: Estourou tempo' + '\n')
            printstyle(c['name'], "Exec in %f: Signal 124: Estourou tempo" % (end - start), c['stderr'])
            return c

        if p.returncode == 137:
            open(c['stderr'], 'w').write('Signal 137: Estourou memória' + '\n')
            printstyle(c['name'], "Exec in %f: Signal 137: Estourou memória" % (end - start), c['stderr'])
            return c

        if p.returncode == 134:
            printstyle(c['name'], "Exec in %f: Signal 134: Abortado" % (end - start), c['stderr'])
            return c

        if not error:
            if os.path.isfile(c['stderr']):
                os.remove(c['stderr'])
            if not validfile(c['stdout']) and not validfile(c['stderr']):
                open(c['stdout'], 'w').write('# Executado sem saída no stdout\n' + '\n')
                printstyle(c['name'], "Exec in %.2fs: code %s (empty stdout)" % ((end - start), p.returncode),
                           c['stdout'])
                return c

        printstyle(c['name'], "Exec in %.2fs: code %s" % ((end - start), p.returncode), c['stdout'])

        return c

